#!/bin/sh

set -x

. ./settings.sh

cd mplayer2

rm -vf mplayer

MAKE_EXE_CMD=$(make V=1)

#echo $MAKE_EXE_CMD

echo "set -x" > makefile_mplayer2_so
echo $MAKE_EXE_CMD | sed 's/.\/version.sh//' | sed 's/\-o mplayer/\-shared \-o libmplayer.so/'  >> makefile_mplayer2_so

chmod +x makefile_mplayer2_so
sh ./makefile_mplayer2_so

ls -lh libmplayer.so mplayer

if [ -f libmplayer.so ] ; then
    $AELF -d libmplayer.so | grep Shared
    $AELF -s libmplayer.so | grep Java
    $AELF -s libmplayer.so | grep ANDROID
    $AELF -s libmplayer.so | grep SDL
fi

cd ..
