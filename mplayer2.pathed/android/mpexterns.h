
#ifndef _MP_EXTERNS_H_
#define _MP_EXTERNS_H_


#ifdef __cplusplus
extern "C" {
#endif

#include "mplayer.h"
#include "mp_core.h"
#include "input/input.h"
#include "version.h"
#include "codec-cfg.h"

    extern struct MPContext *gmpctx;
    
    int isPaused();
 
    // codec
    extern codecs_t *video_codecs;
    extern codecs_t *audio_codecs;
    extern int nr_vcodecs;
    extern int nr_acodecs;
    char * get_codecs_list(int audioflag);
    char * get_mplayer_version ();
   
#ifdef __cplusplus
};
#endif


#endif
