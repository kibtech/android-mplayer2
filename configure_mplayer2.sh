#!/bin/sh

set -x

. ./settings.sh


export CC="${ACC}"
PWD=$(pwd)
AFF_BASE=$PWD/ffmpeg

AC_LDFLAGS="-L${NDK_SYSROOT}/usr/lib -llog -lstdc++"
FFLIBS="avformat swscale postproc avcodec avfilter avresample swresample avutil"
FFLIBS_LDFLAGS=
X264_LDFLAGS="-L${AFF_BASE}/../x264/ -lx264"

for flib in $FFLIBS
do
    echo $flib
    FFLIBS_LDFLAGS="${FFLIBS_LDFLAGS} -L${AFF_BASE}/lib${flib} -l${flib}"
done


######## 
# pkgconfig
# AVPCS=$(find ./ffmpeg -name *.pc | grep -v uninstalled)

# for avpc in $AVPCS
# do
#     echo $avpc;
#     pcn=$(basename $avpc)
#     pcnop=$(echo $pcn | awk -F\. '{print $1}')
#     cat $avpc | sed  s:\${libdir}:${AFF_BASE}\/$pcnop: \
#         | sed s:\${includedir}:${AFF_BASE}:    > ./pkgconfig/$pcn || exit
# done


# exit;
############################

VINCS="-I${PWD}/vincs/sdl -I${PWD}/vincs/mpg123"
VLIBS="-L${PWD}/vlibs -lsdl-1.2 -lmpg123"
export PATH=$PWD/visys:$PATH
export PKG_CONFIG_PATH=$PWD/pkgconfig:$PKG_CONFIG_PATH

ARMX_CFLAGS=$ARM7_CFLAGS
cd mplayer2


./configure \
    --enable-cross-compile \
    --target=arm-linux-eabi \
    --prefix=/data/data/org.witness.sscvideoproto \
    --datadir="/sdcard/mplayer" --confdir="/sdcard/mplayer" --libdir="/sdcard/mplayer"  \
    --disable-iconv \
    --disable-libass \
    --disable-fbdev \
    --disable-radio-v4l2 \
    --disable-tv-v4l1 \
    --disable-tv-v4l2 \
    --disable-v4l2 \
    --enable-libav \
    --enable-libpostproc \
    --enable-sdl \
    --enable-mpg123 \
    --enable-faad   \
    --extra-cflags=" ${ARMX_FLAGS}  -I${AFF_BASE} ${VINCS} " \
    --extra-ldflags="-L${AFF_BASE} ${FFLIBS_LDFLAGS} ${X264_LDFLAGS} ${AC_LDFLAGS} ${VLIBS}"


cd $PWD

